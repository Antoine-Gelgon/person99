from bottle import Bottle,  run, template, route, static_file, get, request
import os, cv2, glob
from turbine.Turbine import *

app = Bottle()
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

@app.route("/output/<filepath:re:.*\.(jpg|jpeg|png|JPG|JPEG|PNG)>")
def imgOut(filepath):
    return static_file(filepath, root="output")

@app.route("/input/<filepath:re:.*\.(jpg|jpeg|png|JPG|JPEG|PNG)>")
def imgIn(filepath):
    return static_file(filepath, root="input")

@app.route("/json/<filepath:re:.*\.json>")
def json(filepath):
    return static_file(filepath, root="json")

@app.route('/')
@app.route('/index')
def index():
    return template('templates/index.tpl')

@app.route('/upload', method='POST')
def upload():
    data = request.files.data
    name, ext = os.path.splitext(data.filename)
    
    if data and data.file:
        if ext not in ('.png','.PNG','.jpg', '.JPG','.jpeg', '.JPEG'):
            return "le fichier n'est pas un .png .jpg .jpeg"
        raw = data.file.read()
        filename = data.filename
        file = open('input/' + filename, 'wb')
        file.write(raw) 
        file.close()

        tb = Turbine()
        im = cv2.imread("input/" + filename)
        (preds, im2) = tb.compute(im)
        print(preds)
        cv2.imwrite("output/" + filename, im2)
        
        file = open('json/' + filename + '.json', 'w')
        file.write(preds) 
        file.close()
        files = []
        files.append(filename)
    return template('templates/galery.tpl', files=files)

    # return '<main><img style="width:50%; float: left" src="output/' + filename + '" /><pre style="width:50%; float: left"><code>' +preds+ '</code></pre></main>' 

@app.route('/galery')
def index():
    dirOuts = glob.glob('output/*')
    files = []
    for out in dirOuts:
        name, ext = os.path.splitext(out)
        files.append(out.split('/')[1])
    return template('templates/galery.tpl', files=files)

run(app, host="0.0.0.0", port=8088, reloader=True, debug=True)
