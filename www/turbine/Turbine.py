from detectron2.utils.logger import setup_logger

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.utils.visualizer import GenericMask
from detectron2.data import MetadataCatalog

import cv2
import random
import numpy as np
import detectron2
import json

class Turbine:
    def __init__(self):
        setup_logger()
        self.cfg = get_cfg()
        # add project-specific config (e.g., TensorMask) here if you're not running a model in detectron2's core library
        self.cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
        # Find a model from detectron2's model zoo. You can use the https://dl.fbaipublicfiles... url as well
        self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
        #ça c'est juste pour nos petits lenovo
        self.cfg.MODEL.DEVICE = "cpu"

    def jsondefault(self,obj):
        if type(obj).__module__ == np.__name__:
            if isinstance(obj, np.ndarray):
                return obj.tolist()
            else:
                return obj.item()
                raise TypeError('Unknown type:', type(obj))

    def getPredictions(self, predictions, metadata, width, height):
        class_names = metadata.get("thing_classes", None)
        boxes = predictions.pred_boxes if predictions.has("pred_boxes") else None
        scores = predictions.scores if predictions.has("scores") else None
        classes = predictions.pred_classes if predictions.has("pred_classes") else None
        masks = np.asarray(predictions.pred_masks)
        masks = [GenericMask(x, height, width) for x in masks]

        labels = None

        output = []

        if classes is not None and class_names is not None and len(class_names) > 1:
            for idx,i in enumerate(classes):
                instance = {}
                label = class_names[i]
                instance['label'] = label
                #instance['box'] = boxes[idx]

                ##print(masks[idx].polygons[0])
                instance['mask'] = masks[idx].polygons
                #print(scores[idx])
                instance['score'] = "{:.0f}%".format(scores[idx] * 100)
                output.append(instance)

        return output



    def compute(self, img):
        (w, h, c) = img.shape
        predictor = DefaultPredictor(self.cfg)

        outputs = predictor(img)
        metadata = MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0])

        v = Visualizer(img[:, :, ::-1], metadata, scale=1.2)
        preds = self.getPredictions(outputs["instances"].to("cpu"), metadata, v.output.width, v.output.height)
        v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
        return(json.dumps(preds, indent=4, default=self.jsondefault), v.get_image()[:, :, ::-1])


'''
turbine = Turbine()
im = cv2.imread("./lesamis.jpg")
(preds, im2) = turbine.compute(im)
print(preds)
cv2.imwrite("./im2.jpg", im2)
'''
