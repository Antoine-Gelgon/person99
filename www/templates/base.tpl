<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<style>
	body,main {
		padding: 0;
		margin: 0;
	}
	.article {
		border-bottom: 1px solid black;
		font-family: monospace;
		font-size: 10px;
		width: 100%;
		display: flex;
	}
	.input, .output {
		width: 50%;
		height: auto;
	}
	img {
		width: 100%;
		height: auto;
	}
</style>
</head>
<body>
<main>	
	{{!base}}
</main>	
</body>
</html>
