% rebase('templates/base.tpl')

% for f in files:
<div class="article">
	<figure class="input">
		<img src='input/{{ f }}'/>
		<figcaption>input/{{ f }}</figcaption>
	</figure>
	<figure class="output">
		<img src='output/{{ f }}'/>
		<figcaption>output/{{ f }}</figcaption>
	</figure>
	<a href="json/{{ f }}.json" >Json</a>
</div>
% end
