#!/usr/bin/env python3
# coding: utf-8


import json
import argparse
import os
parser = argparse.ArgumentParser()

parser.add_argument("--input", "-i", type=str, required=True)
parser.add_argument("--output", "-o", type=str, required=True)

args = parser.parse_args()

files = os.listdir(args.input)

with open('%s/%s' % (args.input, files[0])) as f:
    output = json.load(f)

output["_via_img_metadata"] = {}

for file in files:
    print
    with open('%s/%s' % (args.input, file)) as f:
        data = json.load(f)

    for elem in data["_via_img_metadata"].items():
        output["_via_img_metadata"][elem[0]] = elem[1]

with open(args.output, 'w', encoding='utf-8') as f:
    json.dump(output, f, ensure_ascii=False, indent=4)
